# Weather-O-Clock

## Getting started

```

Versions used for this project -

React version 	- 18.2.0
Node JS version - 16.17.0


To Download the project on local 

	1. Clone the repository:
		git clone https://gitlab.com/tejaskabade1993/weather-o-clock.git

	2. Navigate to the project directory:
		cd weather-o-clock
	
	3. Install dependencies:
		npm i
		NOTE : In case of issues shown after the npm install, run the below to automatically fix the issues:
			npm audit fix
	
	4. Start the development server:
		npm start
	
	
```





