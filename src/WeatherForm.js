import React,{ useState } from "react";

function WeatherForm({ onSearch }) {
    const [input, setInput] = useState('');
    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
        onSearch(input);
        setInput('');
        }
    };

    return (
        <div className="search-bar">
            <input
                type="text"
                className="city-search"
                placeholder="Enter City Name.."
                value={input}
                onChange={(event) => setInput(event.target.value)}
                onKeyPress={handleKeyPress}
            />
        </div>
    );
}

export default WeatherForm;