import React, { useState } from 'react';
import axios from 'axios';
import { Oval } from 'react-loader-spinner';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFrown } from '@fortawesome/free-solid-svg-icons';
import './App.css';
import WeatherForm from './WeatherForm';
import WeatherDisplay from './WeatherDisplay';

function GetWeatherDetails() {
    const [weather, setWeather] = useState({
        loading: false,
        data: {},
        error: false,
    });

    const toDateFunction = () => {
        const months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
        ];
        const WeekDays = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        ];
        const currentDate = new Date();
        const date = `${WeekDays[currentDate.getDay()]} ${currentDate.getDate()} ${months[currentDate.getMonth()]}`;
        return date;
    };

    const search = async (input) => {
        setWeather({ ...weather, loading: true });
        const url = 'https://api.openweathermap.org/data/2.5/weather';
        const api_key = 'f00c38e0279b7bc85480c3fe775d518c';
        try {
            const res = await axios.get(url, {
                params: {
                q: input,
                units: 'metric',
                appid: api_key,
                },
            });
            setWeather({ data: res.data, loading: false, error: false });
            console.log(res.data);
        } catch (error) {
            setWeather({ ...weather, data: {}, error: true });
            // console.log('error', error);
        }
    };

    return (
        <div className="App">
            <h1 className="app-name">Weather O'Clock</h1>
            <WeatherForm onSearch={search} />
            {weather.loading && (
                <>
                <br />
                <Oval type="Oval" color="black" height={100} width={100} />
                </>
            )}
            {weather.error && (
                <>
                <span className="error-message">
                    <FontAwesomeIcon icon={faFrown} />
                    <span style={{ fontSize: '20px' }}>City not found</span>
                </span>
                </>
            )}
            {weather && weather.data && weather.data.main && (
                <WeatherDisplay weather={weather.data} toDateFunction={toDateFunction} />
            )}
        </div>
    );
}

export default GetWeatherDetails;