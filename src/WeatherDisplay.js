function WeatherDisplay({ weather, toDateFunction }) {
    return (
        <div>
        <div className="city-name">
            <h2>
            {weather.name}, <span>{weather.sys.country}</span>
            </h2>
        </div>
        <div className="date">
            <span>{toDateFunction()}</span>
        </div>
        <div className="icon-temp">
            <div className="icon-col">
                <img className="weather-icon" src={`https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`} alt={weather.weather[0].description} />
            </div>
            <div className="temp-col">
                {Math.round(weather.main.temp)}
                <sup className="deg">°C</sup>
            </div>
        </div>
        <div className="des-wind">
            <p>{weather.weather[0].description.toUpperCase()}</p>
            <p>Wind Speed: {weather.wind.speed}m/s</p>
        </div>
        <div className="des-wind">
            <p>Humidity: {weather.main.humidity}%</p>
        </div>
        </div>
    );
}

export default WeatherDisplay;